import { setComponentSharedStyles } from '@cells-components/cells-lit-helpers';
import { css, } from 'lit-element';

setComponentSharedStyles('cells-select-shared-styles', css`
.toggle{
  height:48px !important;
}

.toggle__text {
  text-align:left !important;
  }
 
.option {
  text-align:left !important;
  }
`);

setComponentSharedStyles('bbva-input-field-shared-styles', css`
.input{
  padding-right: 15px;
}
.icon {
  display:none;
}
.message-error__text {
  font-size: 12px;
}
`);