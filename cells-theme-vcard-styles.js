import { setComponentSharedStyles } from '@cells-components/cells-lit-helpers';
import { css, } from 'lit-element';

setComponentSharedStyles('cells-theme-vcard', css `

:host {
  font-family: var(--cells-fontDefault, sans-serif);
}

.body-main-margin-fixed {
  margin-top:60px;
}

@media only screen and (max-width: 480px) {
  .body-main-margin-fixed {
    margin-top:90px;
  } 
}

.body-main {
  width:100%;
  padding-top: 20px;
  min-height: 86vh;
}

.panel-top {
  width: 100%;
  background-color: #f4f4f4;
  border: 1px solid #BDBDBD;
  padding: 10px; }

  .button {
    color: #1973B8;
    background-color: #fff;
    border: 1px solid #D3D3D3;
    padding: 15px;
    height: 48px;
    text-align: center;
    text-decoration: none;
    cursor: pointer;
    font-size: 0.9375rem;
    outline: none;
  }

  .button:active {
    background-color: #f4f4f4;
  }


.bg-grey {
  background-color: #f4f4f4;
}

/*  CARD  */
.card {
  /* Add shadows to create the "card" effect */
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s; }

/* On mouse-over, add a deeper shadow */
.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2); }

/* Add some padding inside the card container */
.container {
  border: 1px solid #dcd9d9;
  border-radius: 4px;
  margin: 0px 20px 15px 20px;
  padding: 15px;
  box-shadow: 1px 1px 3px rgba(0,0,0,.15);
  background: #fff;
}

/*  SECTIONS  */
.section {
  clear: both;
  padding: 0px;
  margin: 0px; }

/*  COLUMN SETUP  */
.col {
  display: block;
  float: left; }

.col:first-child {
  margin-left: 0; }

/*  GROUPING  */
.group:before,
.group:after {
  content: "";
  display: table; }

.group:after {
  clear: both; }

.group {
  zoom: 1;
  /* For IE 6/7 */ }

/*  GRID OF TWELVE  */
.span_12_of_12 {
  width: 100%; }

.span_11_of_12 {
  width: 91.58%; }

.span_10_of_12 {
  width: 83.16%; }

.span_9_of_12 {
  width: 74.75%; }

.span_8_of_12 {
  width: 66.33%; }

.span_7_of_12 {
  width: 57.91%; }

.span_6_of_12 {
  width: 49.5%; }

.span_5_of_12 {
  width: 41.08%; }

.span_4_of_12 {
  width: 32.66%; }

.span_3_of_12 {
  width: 24.25%; }

.span_2_of_12 {
  width: 15.83%; }

.span_1_of_12 {
  width: 7.416%; }

/*  GO FULL WIDTH BELOW 480 PIXELS */
@media only screen and (max-width: 480px) {
  .col {
    margin: 1% 0 1% 0%; }
  .span_1_of_12, .span_2_of_12, .span_3_of_12, .span_4_of_12, .span_5_of_12, .span_6_of_12, .span_7_of_12, .span_8_of_12, .span_9_of_12, .span_10_of_12, .span_11_of_12, .span_12_of_12 {
    width: 100%; } }
 
 /*
 Styles
 */
  cells-select {
    --cells-select-toggle: {
      height: 48px !important;
    }
    --cells-select-options-list: {
      height:auto;
      max-height :250px;
      overflow-y: auto
    }
  }
 
  /**
   * TABS
   */
  .tabs {
    background-color:#fff;
    display: flex;
    flex-wrap: wrap; 
    
  }
  .tabs label {
    order: 1; // Put the labels first
    display: block;
    padding: 0.9rem 2rem 0.7rem 2rem;
    margin-right: 0.25rem;
    cursor: pointer;
    background: #f4f4f4;
    border-top: 1px solid #D3D3D3;
    border-left: 1px solid #D3D3D3;
    border-right: 1px solid #D3D3D3;
    transition: border-top 0.15s;
  }
  .tabs .tab {
    order: 99; // Put the tabs last
    flex-grow: 1;
    width: 100%;
    display: none;
    padding: 10px 0px;
    background: #fff;
    border-top:1px solid #D3D3D3;
  }
  .tabs input[type="radio"] {
    display: none;
  }
  .tabs input[type="radio"]:checked + label {
    background: #fff;
    border-top: 3px solid #49A5E6;
  }
  .tabs input[type="radio"]:checked + label + .tab {
    display: block;
  }
  
  @media (max-width: 45em) {
    .tabs .tab,
    .tabs label {
      order: initial;
    }
    .tabs label {
      width: 100%;
      margin-right: 0;
      margin-top: 0.2rem;
    }
  }

  table {
    width: 100%; 
    border-spacing: 0px;
    border-collapse: separate;
  }
  
  table th {
    padding: 15px;
    background-color: #072146 ;
    color: #fff;
    text-align: left; 
    font-weight: normal;
  }

  .green{
    background-color: #028484; 
  }

  .green th {
    background-color: #028484 !important; 
  }
  
  table td {
    padding: 15px;
    background-color: #fff;
    border-bottom: 1px solid #BDBDBD;
    cursor:pointer;
    font-size:0.9rem;
    color: #666666;
    
  }

  tbody tr:hover td {
    background-color: #f4f4f4;
    border-bottom: 1px solid #999;
  }
 
  .disabled-input {
    opacity: 0.5;
  }
  
.input-number {
  height: 42px;
  background-color: #F4F4F4;
  border-bottom: 1px #333 solid;
  border-top: 0px #F4F4F4 solid;
  border-left: 0px #F4F4F4 solid;
  border-right: 0px #F4F4F4 solid;
  padding-left: 10px;
  padding-top: 5px;
  font-size: 1rem;
}

.content-table-vcard {
  width: 100wh;
  padding-top: 5px;
  overflow-x: auto;
  overflow-y: hidden; 
  padding-bottom:35px;
}

.footer {
  padding: 20px 0 10px;
}

.cells-modal-body {
  padding: 20px;
  font-size: 13px;
  line-height: 1.25em;
  overflow-y: auto;
  max-height: 30rem;
}
.cells-modal-footer {
  padding: 0 20px 20px;
  display: flex;
  align-items: center;
  justify-content: center;
}


.sub-titulo {
  color: #043263;
  font-size: 0.95rem;
  font-weight: bold;
  padding: 10px 10px 0px 5px; }

.table-scroll {
  display: flex;
  flex-flow: column;
  height: 100%;
  width: 100%; }

.table-scroll thead {
  /* head takes the height it requires, 
    and it's not scaled when table is resized */
  flex: 0 0 auto;
  width: calc(100% - 0.9em); }

.table-scroll tbody {
  /* body takes all the remaining available space */
  flex: 1 1 auto;
  display: block;
  overflow-y: auto; }

.table-scroll tbody tr {
  width: 100%; }

.table-scroll thead,
.table-scroll tbody tr {
  display: table;
  table-layout: fixed; }

.table-scroll tbody {
  background-color: #fff; }

`);
